#include<iostream>
#include<cstring>
class MyString
{
    int s_len;
    char* s_buf;
public:
    MyString();
    MyString(const char*);
    MyString(const MyString&);
    ~MyString();
    friend std::ostream &operator<<(std::ostream &,const MyString &);
};

MyString::MyString():s_len(0),s_buf(nullptr){
}
MyString::MyString(const char * buf)
{
    s_len=strlen(buf);
    s_buf=new char(s_len+1);
    strcpy(s_buf,buf);
}
MyString::MyString(const MyString& ref):s_len(ref.s_len)
{
    s_buf=new char(s_len+1);
    strcpy(s_buf,ref.s_buf);
}
MyString::~MyString()
{
    if(s_len>0)
    {
        delete[] s_buf;
        s_buf=NULL;
    }
}
std::ostream &operator<<(std::ostream &rout,const MyString &ref)
 {
    rout << ref.s_buf<< "\n";
    return rout;

 }