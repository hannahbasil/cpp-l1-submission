#include"Mypoint.h"
#include<iostream>
int main()
{
    MyPoint<int> p1(3, 4);
    int d1 = p1.distanceFromOrigin();
    std::cout << d1 << "\n";
    p1.display();
    Quadrant qr = p1.getQuadrant();
    std::cout << qr << "\n";
    p1.move(2, 3);      //Point will move from (3,4) to (5,8)
	MyPoint<int> p2;   //Assume (1,1) as default position
    p2.move(3, 4);     //Will move from (1,1) to (4,5)
    qr = p2.getQuadrant();
    std::cout << qr << "\n";
    p2.display();
	MyPoint<float> p3(-2.3f, -4.8f);
    qr = p3.getQuadrant();
    std::cout << qr << "\n";
 	return 0;
 	
}