#include<iostream>
#include<math.h>
enum Quadrant { Q1 = 1, Q2 = 2, Q3 = 3, Q4 = 4 };
template <class Point>
class MyPoint {
    Point a;
    Point b;
public:
    MyPoint();
    MyPoint(Point a1, Point b1);
    Point distanceFromOrigin()const;
    Quadrant getQuadrant()const;
    void move(Point, Point);
    void display()const;
};


template <class Point>
MyPoint<Point>::MyPoint():a(1),b(1){
}

template <class Point>
MyPoint<Point>::MyPoint(Point a1, Point b1):a(a1),b(b1){
}

template <class Point>
Point MyPoint<Point>::distanceFromOrigin()const
{
    return sqrt(pow(a,2) + pow(b,2));
}

template <class Point>
Quadrant MyPoint<Point>::getQuadrant()const
{
    if (a > 0 && b > 0) 
        return Q1;
	else if (a < 0 && b>0) 
        return Q2;
    else if (a < 0 && b < 0) 
        return Q3;
    else 
        return Q4;
}

template <class Point>
void MyPoint<Point>::move(Point a1, Point b1)
{
    a = a + a1;
	b = b + b1;
}


template <class Point>
void MyPoint<Point>::display()const
{
    std::cout << "("<< a<<","<< b<<")" << std::endl;
}
 
   