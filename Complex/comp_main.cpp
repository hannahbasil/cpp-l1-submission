#include "comp_h.h"

int main()
{
	complex <int> c1(12,8);
	complex <int> c2(10,6);
	complex <int> c3;
	std::cout<<"Displaying results of integer values"<<"\n";
	c3=c1 + c2;
	std::cout<<"Addition: "<<"\n";
	c3.display();
	c3=c1 - c2;
	std::cout<<"Subtraction: "<<"\n";
	c3.display();
	c3=c1 * c2;
	std::cout<<"Multiplication: "<<"\n";
	c3.display();
	
	complex <int> c7(c1);
	std::cout<<"Equality status between two objects with integer values: "<<"\n";
	if(c7==c1)
	{
		std::cout<<"Equal"<<"\n";
	}
	
	complex <double> c4(10.4,9.6);
	complex <double> c5(4.3,5.5);
	complex <double> c6;
	std::cout<<"Displaying results of float values"<<"\n";
	c6=c4 + c5;
	std::cout<<"Addition: "<<"\n";
	c6.display();
	c6=c4 - c5;
	std::cout<<"Subtraction: "<<"\n";
	c6.display();
	c6=c4 * c5;
	std::cout<<"Multiplication: "<<"\n";
	c6.display();
	
	return 0;
}
