#include<iostream>
#include<iomanip>
class Time 
{
	int t_hr;
	int t_min;
	int t_sec;
public:
	Time();
    Time(int,int,int);
    Time(const Time &);
    Time operator +(const Time &)const;
	Time operator +(const int &)const;
	Time operator ++();
	bool operator ==(const Time &)const;
	void display()const;
};

Time:: Time():t_hr(0),t_min(0),t_sec(0){
}

Time::Time(int hr,int min,int sec):t_hr(hr),t_min(min),t_sec(sec){
}

Time::Time(const Time &ref):t_hr(ref.t_hr),t_min(ref.t_min),t_sec(ref.t_sec){ 
}

Time overflow(int hour,int mins,int secs)
{
	if(secs>=60)
	{
		secs-=60;
		mins++;
	}
	if(mins>=60)
	{
		mins-=60;
		hour++;
	}	
	return Time(hour,mins,secs);
}

Time Time::operator +(const Time& ref)const
{
	int hr,min,sec;
	sec=t_sec+ ref.t_sec;
	min=t_min+ref.t_min;
	hr=t_hr+ref.t_hr;
	return overflow(hr,min,sec);
} 

Time Time::operator +(const int& val)const
{
	int hr,min,sec;
	hr=t_hr;
	min=t_min;
	sec=t_sec+val;
	return overflow(hr,min,sec);
}

Time Time::operator ++()
{
	++t_sec;
	return *this;
}

bool Time::operator ==(const Time& ref)const
{
	if(t_hr==ref.t_hr&&t_min==ref.t_min&&t_sec==ref.t_sec)
		return true;
	return false;
}

void Time::display()const
{
	std::cout<<std::setfill('0')<<std::setw(2);
	std::cout<<t_hr<<":";
	std::cout<<std::setfill('0')<<std::setw(2);
	std::cout<<t_min<<":";
	std::cout<<std::setfill('0')<<std::setw(2);
	std::cout<<t_sec<<std::endl;
}