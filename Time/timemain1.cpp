#include<iostream>
#include"time1.h"
int main() {
Time t1(10,20,15);
Time t2(1, 10, 05);
Time t3; // Time will be 00:00:00
t3 = t1 + t2; // t3 will be 11:30:20
t3.display();
Time t4;
t4 = t1 + 50; // 50 seconds will be added, t4 will be 10:21:05
t4.display();
++t1; // t1 will be 10:20:16
t1.display();
(++t2).display(); // t2 will be 1:10:6, same will be displayed
t4=t1;
if(t1 == t2)
	std::cout<<"Equal"<<std::endl;
else
	std::cout<<"Not Equal"<<std::endl;
/*std::cout &lt;&lt; t1 &lt;&lt; &quot;\n&quot;;
*/
return 0;
}