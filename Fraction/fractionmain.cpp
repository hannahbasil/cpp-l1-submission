#include"fraction.h"
int main() {
  Fraction f1(2,3);
  Fraction f2(3,4);
  Fraction f3=f1 + f2; 
  std::cout<<f3<<"\n";
  Fraction f4=f1 - f2;
  std::cout<<f4<<"\n";
  Fraction f5= f1 * f2;
  std::cout<<f5<<"\n";
  Fraction f6 =f1 + 5 ;        // 5 + 2/3
  std::cout<<f6<<"\n";
  Fraction f7=f1 * 2;
  std::cout<<f7<<"\n";
  if(f1 == f2)
    std::cout<<"equal\n";
  else
    std::cout<<"Not Equal\n";
  return 0;
}