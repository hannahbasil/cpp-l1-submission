#include<iostream>
class Currency {
  int c_rupees; 
  int c_paisa;
  public:
  Currency();
  Currency(int,int);
  Currency(const Currency&);
  Currency operator+(const Currency&);
  Currency operator-(const int);
  Currency operator=(const Currency&);
  Currency operator+(const int);
  Currency operator++();
  bool operator==(const Currency&);
  void display()const;
  friend std::ostream &operator<<(std::ostream&,const Currency&);
};