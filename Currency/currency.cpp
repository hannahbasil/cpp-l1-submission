#include<iostream>
#include"currency.h"

Currency verify(int r,int p)
{
    if(p>=100)
    {
        r=r+p/100;
        p=p%100;
    }
    return Currency(r,p);

}

Currency::Currency():c_rupees(0),c_paisa(0){
}

Currency::Currency(int rupees,int paisa):c_rupees(rupees),c_paisa(paisa){
}
Currency::Currency(const Currency& ref):c_rupees(ref.c_rupees),c_paisa(ref.c_paisa){
}

Currency Currency::operator+(const Currency& ref)
{
    int r,p;
    r=ref.c_rupees+c_rupees;
    p=ref.c_paisa+c_paisa;
    return verify(r,p);
}

Currency Currency::operator-(const int paisa)
{
    int r,p;
    r=c_rupees;
    p=c_paisa-paisa;
    return Currency(r,p);

}

Currency Currency::operator=(const Currency& ref)
{
    if(this==&ref)
        return *this;
    c_rupees=ref.c_rupees;
    c_paisa=ref.c_paisa;
    return *this;
}


Currency Currency::operator++()
{
    c_paisa++;
    return verify(c_rupees,c_paisa);
}

Currency Currency::operator+(const int paisa)
{
    int r,p;
    r=c_rupees;
    p=c_paisa+paisa;
    return verify(r,p); 
}

bool Currency::operator==(const Currency&ref)
{
      return c_paisa==ref.c_paisa&&c_rupees&&ref.c_rupees;
}

void Currency:: display()const{
    std::cout<<c_rupees<<"."<<c_paisa<<std::endl;
}
std::ostream &operator<<(std::ostream& rout,const Currency& ref)
 {
    rout << ref.c_rupees<<"."<<ref.c_paisa<< "\n";
    return rout;

 } 

