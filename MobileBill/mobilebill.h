#include<iostream>
#include<string>
class Customer 
{
private:
	int c_customerid;
	std::string c_name;
  	std::string c_phno;
public:
	Customer(int id,std::string name,std::string phno):c_customerid(id),c_name(name),c_phno(phno){
  	}
  	virtual void display() const;
  	virtual void makeCall(int)=0;
  	virtual void credit(double){
  	}
  	virtual void billPay(double){
	}
  	virtual double getBalance()const{
	}
};


void Customer:: display() const
{
  	std::cout<<"Customer ID: "<<c_customerid<<std::endl<<"Name:"<<c_name<<std::endl<<"Mobile number:"<<c_phno<<std::endl;
}

class PostpaidCustomer :public Customer
{
	double c_balance;
public:
  	PostpaidCustomer(int id,std::string name,std::string phno,double bal):Customer(id,name,phno),c_balance(bal){
  	}
  	PostpaidCustomer(int id,std::string name,std::string phno):Customer(id,name,phno),c_balance(0){
  	}
  	void makeCall(int);
  	double getBalance()const;
  	void billPay(double);
  	void display()const;  
};  

void  PostpaidCustomer::makeCall(int n)
{
	c_balance-=n;
}
double PostpaidCustomer::getBalance()const
{
	return c_balance;
}
void PostpaidCustomer::billPay(double amt)
{
	c_balance-=amt;
}
void PostpaidCustomer::display()const
{
	Customer::display();
	std::cout<<"Balance:"<<c_balance<<std::endl;
}


class PrepaidCustomer :public Customer
{
	double c_balance;
public:
  	PrepaidCustomer(int id,std::string name,std::string phno,double bal):Customer(id,name,phno),c_balance(bal){
  	}
  	PrepaidCustomer(int id,std::string name,std::string phno):Customer(id,name,phno),c_balance(100){
  	}
  	void makeCall(int);
  	double getBalance()const;
  	void credit(double);
  	void display()const;  
};  

void  PrepaidCustomer::makeCall(int n)
{
	c_balance-=n;
}
double PrepaidCustomer::getBalance()const
{
	return c_balance;
}
void PrepaidCustomer::credit(double amt)
{
	c_balance+=amt;
}
void PrepaidCustomer::display()const
{
	Customer::display();
	std::cout<<"Balance:"<<c_balance<<std::endl;
}



