#include<iostream>
#include<string>

int main() {

  PostpaidCustomer pobj(1001, "Richard", "9845012345");
  pobj.display();
  Customer *ptr = new PostpaidCustomer(1001, "Scott", "98223 12345", 500);
  ptr->makeCall(5);    //should invike from derived class
  ptr->billPay(100);  //should invoke from derived class
  int bal = ptr->getBalance();  
  std::cout << "Balance is :" << bal << "\n";
  ptr->display();
  PrepaidCustomer nobj(1005, "Robin", "9845012335");
  nobj.display();
  Customer *ptr2 = new PrepaidCustomer(1003, "Meyers","9845012346",200 );
  ptr2->makeCall(5);    //should invike from derived class
  ptr2->credit(100);  //should invoke from derived class
  int bal2 = ptr2->getBalance();  
  std::cout << "Balance is :" << bal2 << "\n";
  ptr2->display();
  delete ptr;
}