#include<iostream>
#include<cmath>

int d_feet,d_inches;

class Distance{
    int d_feet;
    int d_inches;
    public:
        Distance();
        Distance(int,int);
        Distance(const Distance &ref);
        Distance operator+(const Distance &ref);
        Distance operator-(const Distance &ref);
        bool operator==(const Distance &ref);
        Distance operator++();
        Distance operator+(int);
        bool operator<(const Distance &ref);
        bool operator>(const Distance &ref);
        void check();
        friend std::ostream &operator<<(std::ostream &,const Distance&);
};

Distance::Distance():d_feet(0),d_inches(0){}
Distance::Distance(int ft,int inch):d_feet(ft),d_inches(inch){}
Distance::Distance(const Distance &ref):d_feet(ref.d_feet),d_inches(ref.d_inches){}
Distance Distance::operator+(const Distance &ref){
     d_feet=d_feet+ref.d_feet;
     d_inches=d_inches+ref.d_inches;
     check();
     return Distance (d_feet,d_inches);
}
Distance Distance::operator-(const Distance &ref){
     d_feet=d_feet-ref.d_feet;
     d_inches=d_inches-ref.d_inches;
     check();
     return Distance(d_feet,d_inches);
}
bool Distance::operator==(const Distance &ref){
    if(d_feet==ref.d_feet && d_inches==ref.d_inches)
        return true;
    else
        return false;
}
Distance Distance::operator+(int val){
        d_feet=d_feet;
        d_inches=d_inches+val;
        check();
        return Distance(d_feet,d_inches);
    }    
Distance Distance::operator++()
    {
        ++d_inches;
        check();
        return *this;
    }
bool Distance::operator<(const Distance &ref){
    return d_feet<ref.d_feet && d_inches<ref.d_inches;

}   
bool Distance::operator>(const Distance &ref){
    return d_feet>ref.d_feet && d_inches>ref.d_inches;

}    
void Distance::check()
{
    if(d_inches>=12){
		    d_feet += d_inches/12;
		    d_inches = (d_inches % 12);

    }
    else if(d_inches<0){
		    d_feet -= (abs(d_inches)/12 + 1);
		    d_inches = 12 - (abs(d_inches) % 12);
    }
}
std::ostream &operator<<(std::ostream &rout,const Distance&ref)
{
    rout<<ref.d_feet<<"."<<ref.d_inches<<"\n";
    return rout;
}          