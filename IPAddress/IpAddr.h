#include<string>
#include<iostream>
#include<sstream>
class IPaddr
{
	int block_a;
	int block_b;
	int block_c;
	int block_d;
	std::string i_addr;
	public:
		IPaddr(int,int,int,int);
		IPaddr();
		IPaddr(std::string);
		void display()const;
};

IPaddr::IPaddr(int a,int b,int c,int d):block_a(a),block_b(b),block_c(c),block_d(d)
    {
		std::stringstream ss;
		ss<<block_a<<"."<<block_b<<"."<<block_c<<"."<<block_d;
		i_addr=ss.str(); 	
    }
IPaddr::IPaddr():block_a(127),block_b(0),block_c(0),block_d(1),i_addr("127.0.0.1"){
	}
IPaddr::IPaddr(std::string addr):i_addr(addr)
    {
		std::stringstream ss;
		char ch;
		ss>>block_a>>ch>>block_b>>ch>>block_c>>ch>>block_d;
	}
void IPaddr::display()const
	{
		std::cout<<i_addr<<std::endl;
	}