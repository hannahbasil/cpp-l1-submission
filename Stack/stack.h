#include<iostream>
const int SIZE=10;
template<class T>
class Stack
{
    private:
        int s_tos;
        T stack[SIZE];
    public:
        Stack():s_tos(0){}
        void push(int);
        T pop();
        T peek();
};

template<class T>
void Stack<T>::push(int val){
    if(s_tos==SIZE)
        std::cout<<"Stack is full"<<"\n";
    else
    {
        stack[s_tos]=val;
        s_tos++;
    }
}
template<class T>
T Stack<T>::pop(){
    if(s_tos==0){
        std::cout<<"Stack is empty"<<"\n";
        return 0;
    }
    else
    {
        s_tos--;
        return stack[s_tos];
    }
}
template<class T>
T Stack<T>::peek(){
    if(s_tos==0){
        std::cout<<"Stack is empty"<<"\n";
        return 0;
    }
    else
    {
        return stack[s_tos];
    }
} 